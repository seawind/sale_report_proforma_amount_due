# -*- coding: utf-8 -*-
{
    'name': "sale_report_proforma_amount_due",

    'summary': """
        Adds the 'Amount due' line in the pro-forma invoice""",

    'description': """
        Adds the 'Amount due' line in the pro-forma invoice after the Total line.
	Estimates the earliest amount to receive according to the payment terms if those are defined.
    """,

    'author': "Magboard LLC",
    'website': "",
    'category': 'Sale',
    'version': '11.0.1.0.0',
    'depends': ['base','sale'],
    'data': ['report/sale_report_proforma_amount_due.xml',
            ],
    'installable': True   
}
