# -*- coding: utf-8 -*-

from odoo import api, models, _


class SaleOrderAmountDue(models.AbstractModel):    
    _inherit='sale.order'

    @api.multi    
    def _get_earliest_due_payment(self):
        """
        This function gets the earliest amount to receive according to the payment terms.
        """
        options={"day_after_invoice_date":0,"last_day_current_month":1,"fix_day_following_month":2,"last_day_following_month":3}
        when=""
        if (len(self.payment_term_id.line_ids)==1 and self.payment_term_id.line_ids[0].value=='balance') or len(self.payment_term_id.line_ids)==0:
                amount_due=self.amount_total          
        else:
            terms=self.payment_term_id.line_ids    
            #notBalance=terms.search(['&',('value', '!=', 'balance'),('id','in',tuple(terms.ids))], order='days asc')         
            termsl=[(t.value,t.value_amount,t.days, options[t.option]) for t in terms]            
            termsl.sort(key=lambda tup: (tup[3],tup[2]))
            terms=[t for t in termsl if t[3]==termsl[0][3] and t[2]==termsl[0][2]]
            amount_due=0
            for term in terms:
                if term[0]=="balance":
                    amount_due=self.amount_total
                    break
                elif term[0]=="percent":
                    amount_due+=float(term[1])*float(self.amount_total)/100
                elif term[0]=="fixed":
                    amount_due+=float(term[1])
            if terms[0][2]==0 and terms[0][3]==0:
                when=_("Now")
        return when,amount_due

       

